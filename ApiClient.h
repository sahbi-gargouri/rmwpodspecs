//
//  ApiRequests.h
//  RMWCaissePro
//
//  Created by GARGOURI Sahbi on 29/10/2015.
//  Copyright © 2015 BNP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@protocol ApiClientDelegate;

@interface ApiClient : AFHTTPSessionManager

@property (nonatomic, weak) id<ApiClientDelegate>delegate;

@property (nonatomic) NSString *access_token;
@property (nonatomic) NSString *expires_in;
@property (nonatomic) NSString *token_type;
@property (nonatomic) double timeTokenGet;

@property (nonatomic) NSString *apiBaseUrl;
@property (nonatomic) NSString *merchantName;
@property (nonatomic) NSString *merchantPassword;
@property (nonatomic) NSString *merchantEntrepriseCode;
@property (nonatomic) NSString *merchantConsumerKey;
@property (nonatomic) NSString *merchantConsumerSecret;
@property (nonatomic) NSString *merchantTillID;
@property (nonatomic) NSString *merchantStoreID;
@property (nonatomic) NSString *merchantMCC;
@property (nonatomic) NSString *merchantPSPID;
@property (nonatomic) NSString *merchantPSPMerchantID;


// Initialisation Methods
+ (ApiClient *)sharedClient;
- (instancetype)initWithBaseURL:(NSURL *)url;

- (void)setMerchantValuesWithName:(NSString *)merchantName merchantPassword:(NSString *)merchantPassword merchantEntrepriseCode:(NSString *)merchantEntrepriseCode merchantConsumerKey:(NSString *)merchantConsumerKey merchantConsumerSecret:(NSString *)merchantConsumerSecret merchantTillID:(NSString *)merchantTillID merchantStoreID:(NSString *)merchantStoreID merchantMCC:(NSString *)merchantMCC merchantPSPID:(NSString *)merchantPSPID merchantPSPMerchantID:(NSString *)merchantPSPMerchantID apiBaseUrl:(NSString *)apiBaseUrl;

- (void)callPOST:(NSString *)path withParams:(NSDictionary *)params;
- (void)getToken:(id)delegate;
- (void)openTicket:(id)delegate withId:(NSString *)ticketId rmwId:(NSString *)rmwId;
- (void)performPayment:(id)delegate withTotalAmountBeforeDiscount:(float)totalAmount loyaltyAmount:(float)loyaltyAmount couponsAmount:(float)couponsAmount ticketId:(NSString *)ticketId rmwId:(NSString *)rmwId merchantPaymentId:(NSString *)merchantPaymentId;
- (void)cancelIdentification:(id)delegate withTicketId:(NSString *)ticketId rmwId:(NSString *)rmwId;
- (void)checkPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId;
- (void)closeTicket:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPeymentId amount:(float)amount rmwId:(NSString *)rmwId advantageListUsed:(NSArray *)advantageListUsed transactionId:(NSString *)transactionId transactionDate:(NSDate *)transactionDate;
- (void)cancelPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId;

@end

#pragma mark - Protocol Methods

@protocol ApiClientDelegate <NSObject>

@optional

// RETDocumentsTableViewController
- (void)apiClient:(ApiClient *)sharedClient didSucceedWithResponse:(id)responseObject;
- (void)apiClient:(ApiClient *)sharedClient didSucceedWithOperations:(NSArray *)operations;
- (void)apiClient:(ApiClient *)sharedClient didFailWithError:(NSError *)error;

@end