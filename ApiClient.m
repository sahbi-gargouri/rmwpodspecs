//
//  ApiRequests.m
//  RMWCaissePro
//
//  Created by GARGOURI Sahbi on 29/10/2015.
//  Copyright © 2015 BNP. All rights reserved.
//

#import "ApiClient.h"
#import "AFHTTPRequestOperation.h"

static NSString * const API_KEY = @"aXh0OTlyYmFLWDZObzBkOFg3QXVudjlNelM0YTpld2prbmZXSmo2aXVzTWRDZjg5a3Y5ZERhWVVh";
static NSString * const grant_type = @"password";

static NSString * const TOKEN_PATH   = @"sso/token";
static NSString * const OPEN_TICKET   = @"pay-mlv/2.1/proximity/ticket/open";
static NSString * const PERFORM_PAYMENT   = @"pay-tm/2.1/proximity/payment/perform";
static NSString * const CHECK_PAYMENT   = @"pay-tm/2.1/proximity/payment/check";
static NSString * const CANCEL_PAYMENT   = @"pay-tm/2.1/proximity/payment/cancel";
static NSString * const CLOSE_TICKET   = @"pay-tm/2.1/proximity/ticket/close";
static NSString * const CANCEL_TICKET   = @"pay-tm/2.1/proximity/ticket/cancel";
static NSString * const CANCEL_IDENTIFICATION   = @"pay-tm/2.1/proximity/identification/cancel";


@implementation ApiClient

static ApiClient *_sharedClient = nil;


+ (ApiClient *)sharedClient
{
    static ApiClient *_sharedHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:_sharedHTTPClient.apiBaseUrl]];//[[NSUserDefaults standardUserDefaults] objectForKey:@"RMW	serverURL"]
    });
    
    return _sharedHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        //self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)setMerchantValuesWithName:(NSString *)merchantName merchantPassword:(NSString *)merchantPassword merchantEntrepriseCode:(NSString *)merchantEntrepriseCode merchantConsumerKey:(NSString *)merchantConsumerKey merchantConsumerSecret:(NSString *)merchantConsumerSecret merchantTillID:(NSString *)merchantTillID merchantStoreID:(NSString *)merchantStoreID merchantMCC:(NSString *)merchantMCC merchantPSPID:(NSString *)merchantPSPID merchantPSPMerchantID:(NSString *)merchantPSPMerchantID apiBaseUrl:(NSString *)apiBaseUrl {
    self.merchantName = merchantName;
    self.merchantPassword = merchantPassword;
    self.merchantEntrepriseCode = merchantEntrepriseCode;
    self.merchantConsumerKey = merchantConsumerKey;
    self.merchantConsumerSecret = merchantConsumerSecret;
    self.merchantMCC = merchantMCC;
    self.merchantTillID = merchantTillID;
    self.merchantStoreID = merchantStoreID;
    self.merchantPSPID = merchantPSPID;
    self.merchantPSPMerchantID = merchantPSPMerchantID;
    self.apiBaseUrl = apiBaseUrl;
    NSLog(@"name :%@, psw :%@, code :%@, mcc :%@, till :%@, storeID :%@, pspID :%@, pspMe :%@, baseUrl :%@", merchantName, merchantPassword, merchantEntrepriseCode, merchantMCC, merchantTillID, merchantStoreID, merchantPSPID, merchantPSPMerchantID, apiBaseUrl);
}

- (void)getToken:(id)delegate {
    self.delegate = delegate;
    NSDictionary *params = @ {@"grant_type" :grant_type, @"username" :self.merchantName, @"password" :self.merchantPassword};

    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    // Create NSData object
    NSData *nsdata = [[NSString stringWithFormat:@"%@:%@", self.merchantConsumerKey, self.merchantConsumerSecret] dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *apiKey = [nsdata base64EncodedStringWithOptions:0];
    [self.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@", apiKey] forHTTPHeaderField:@"Authorization"];
    [self.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type" ];
    
    [self POST:TOKEN_PATH parameters:params
       success:^(NSURLSessionDataTask *operation, id responseObject) {
           NSLog(@"JSON: %@ \n access_token :%@ \n token_type :%@ \n expires_in :%@", responseObject, responseObject[@"access_token"], responseObject[@"token_type"], responseObject[@"expires_in"]);
           self.access_token = responseObject[@"access_token"];
           self.token_type = responseObject[@"token_type"];
           self.expires_in = responseObject[@"expires_in"];
           self.timeTokenGet = [[NSDate date] timeIntervalSince1970];
           if ([self.delegate respondsToSelector:@selector(apiClient:didSucceedWithResponse:)]) {
               [self.delegate apiClient:self didSucceedWithResponse:responseObject];
           }
       }
       failure: ^(NSURLSessionDataTask *operation, NSError *error) {
           NSLog(@"Error: %@", error);
           if ([self.delegate respondsToSelector:@selector(apiClient:didFailWithError:)]) {
               [self.delegate apiClient:self didFailWithError:error];
           }
       }];
}


- (BOOL)isTokenExpired {
    double currentTime = [[NSDate date] timeIntervalSince1970];
    NSLog(@"1:%g 2:%g 3:%g", currentTime, self.timeTokenGet, [self.expires_in doubleValue]);
    if (currentTime - self.timeTokenGet > [self.expires_in doubleValue]) {
        return YES;
    }
    return NO;
}

- (void)callPOST:(NSString *)path withParams:(NSDictionary *)params {
    
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    NSLog(@"%@", [NSString stringWithFormat:@"%@:%@", self.token_type, self.access_token]);
    [self.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@", @"Bearer", self.access_token] forHTTPHeaderField:@"Authorization"];
    NSLog(@"params :%@", params);
    [self POST:path parameters:params
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              NSLog(@"success :%@", responseObject);
              if ([self.delegate respondsToSelector:@selector(apiClient:didSucceedWithResponse:)]) {
                  [self.delegate apiClient:self didSucceedWithResponse:responseObject];
              }
          }
          failure: ^(NSURLSessionDataTask *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              if ([self.delegate respondsToSelector:@selector(apiClient:didFailWithError:)]) {
                  [self.delegate apiClient:self didFailWithError:error];
              }
          }
     ];
}


- (void)openTicket:(id)delegate withId:(NSString *)ticketId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"12345678910" forKey:@"invocationId"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:@ {@"ticketId" :ticketId, @"orderId" :@"05"} forKey:@"ticket"];
    [parameters setValue:@ {@"subscriberCode" :rmwId, @"identificationType" :@"01"} forKey:@"customer"];
    [parameters setValue:@ {@"posId" :@"2", @"enterpriseCode" :self.merchantEntrepriseCode, @"tillId" :self.merchantTillID, @"storeId" :self.merchantStoreID} forKey:@"merchant"];
    [parameters setValue:@[] forKey:@"categoryList"];
    NSLog(@"%@", parameters);
    [self callPOST:OPEN_TICKET withParams:parameters];
}

- (void)performPayment:(id)delegate withTotalAmountBeforeDiscount:(float)totalAmount loyaltyAmount:(float)loyaltyAmount couponsAmount:(float)couponsAmount ticketId:(NSString *)ticketId rmwId:(NSString *)rmwId merchantPaymentId:(NSString *)merchantPaymentId {
    self.delegate = delegate;
    float netAmount = totalAmount - loyaltyAmount - couponsAmount;
    
    NSString *netAmountNormalized = [[NSString stringWithFormat:@"%.2f", netAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *totalAmountNormalized = [[NSString stringWithFormat:@"%.2f", totalAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *loyaltyAmountNormalized = [[NSString stringWithFormat:@"%.2f", loyaltyAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *couponsAmountNormalized = [[NSString stringWithFormat:@"%.2f", couponsAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :self.merchantEntrepriseCode, @"mcc": self.merchantMCC, @"pspId": self.merchantPSPID, @"pspMerchantId": self.merchantPSPMerchantID} forKey:@"merchant"];
    [parameters setValue:@ {@"amountToPay" :netAmountNormalized, @"merchantPaymentId" :merchantPaymentId, @"purchaseDate" :stringDate} forKey:@"payment"];
    [parameters setValue:@ {@"captureDate" :stringDate, @"operationType" :@"CAP"} forKey:@"pspOptions"];
    [parameters setValue:@ {@"currency" :@"EUR", @"exponent" :@"2", @"netAmount" :netAmountNormalized, @"orderId" :@"05", @"remainingAmount" :netAmountNormalized, @"rmwCouponAmount" :couponsAmountNormalized, @"rmwLoyaltyAmount" :loyaltyAmountNormalized, @"ticketId" :ticketId, @"totalAmount" :totalAmountNormalized} forKey:@"ticket"];
    [self callPOST:PERFORM_PAYMENT withParams:parameters];
}

- (void)checkPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"ticketId" :ticketId} forKey:@"ticket"];
    [parameters setValue:@ {@"enterpriseCode" :self.merchantEntrepriseCode} forKey:@"merchant"];
    [parameters setValue:@ {@"merchantPaymentId" :merchantPaymentId} forKey:@"payment"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [self callPOST:CHECK_PAYMENT withParams:parameters];
}


- (void)cancelPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"12345678910" forKey:@"invocationId"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"merchantPaymentId" :merchantPaymentId} forKey:@"payment"];
    [parameters setValue:@ {@"ticketId" :ticketId} forKey:@"ticket"];
    [parameters setValue:@ {@"enterpriseCode" :self.merchantEntrepriseCode} forKey:@"merchant"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [self callPOST:CANCEL_PAYMENT withParams:parameters];
}

- (void)closeTicket:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPeymentId amount:(float)amount rmwId:(NSString *)rmwId advantageListUsed:(NSArray *)advantageListUsed transactionId:(NSString *)transactionId transactionDate:(NSDate *)transactionDate {

    NSString *amountNormalized = [[NSString stringWithFormat:@"%.2f", amount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    NSString *stringTransactionDate = [df stringFromDate:transactionDate];

    self.delegate = delegate;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    NSArray *transactions = [NSArray arrayWithObjects:
                        @ {@"merchantPaymentId" :merchantPeymentId, @"transactionId" :transactionId, @"transactionDate" :stringTransactionDate, @"amountPaid" :amountNormalized, @"exponent" :@"2", @"currency" :@"EUR", @"tagNonRMWPayment" :@0, @"transactionType" :@"00" }, nil];
    [parameters setValue:transactions forKey:@"transactionList"];
    [parameters setValue:@"OK" forKey:@"merchantFeedback"];
    [parameters setValue:stringTransactionDate forKey:@"merchantActionDate"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :self.merchantEntrepriseCode} forKey:@"merchant"];
    [parameters setValue:@ {@"ticketId" :ticketId, @"orderId" :@"05", @"netAmount" :amountNormalized, @"currency" :@"EUR", @"exponent" :@"2"} forKey:@"ticket"];
    [parameters setValue:@1 forKey:@"degratedMode"];
    [parameters setValue:advantageListUsed forKey:@"advantageListUsed"];
    [self callPOST:CLOSE_TICKET withParams:parameters];
}

- (void)cancelTicket:(id)delegate withAmount:(float)amount ticketId:(NSString *)ticketId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:stringDate forKey:@"merchantActionDate"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:@1 forKey:@"degratedMode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"ticketId" :@"NAME_12_9876", @"orderId" :@"05"} forKey:@"ticket"];
    [parameters setValue:@ {@"subscriberCode" :@"3614569818746123456"} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :self.merchantEntrepriseCode, @"tillId" :self.merchantTillID, @"storeId" :self.merchantStoreID} forKey:@"merchant"];
    [self callPOST:CANCEL_TICKET withParams:parameters];
}

- (void)cancelIdentification:(id)delegate withTicketId:(NSString *)ticketId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:stringDate forKey:@"merchantActionDate"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:@1 forKey:@"degratedMode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"ticketId" :ticketId, @"orderId" :@"05"} forKey:@"ticket"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :self.merchantEntrepriseCode, @"tillId" :self.merchantTillID, @"storeId" :self.merchantStoreID} forKey:@"merchant"];
    [self callPOST:CANCEL_IDENTIFICATION withParams:parameters];
}


@end
